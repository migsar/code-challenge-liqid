import React from 'react';

import { TextAnswer } from '../text-answer/text-answer';
import { DropdownAnswer } from '../dropdown-answer/dropdown-answer';
import { RadioAnswer } from '../radio-answer/radio-answer';

export const Answer = ({type, data, onAnswer}) => {
  switch(type){
    case 'text':
      return <TextAnswer onAnswer={onAnswer} />;
    case 'dropdown':
      return <DropdownAnswer data={data} onAnswer={onAnswer} />;
    case 'radio':
      return <RadioAnswer data={data} onAnswer={onAnswer} />;
    default:
      return <h1>Error</h1>; // todo: improve error
  }
};
