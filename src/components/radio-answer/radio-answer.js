import React, { Fragment } from 'react';

export const RadioAnswer = ({data}) => (
  <fieldset>
    {data.options.map(option => (
      <Fragment key={option.id}>
        <input type="radio" id={option.id} name={data.id} value={option.value} />
        <label htmlFor={option.id}>{option.name}</label>
      </Fragment>
    ))}
  </fieldset>
);
