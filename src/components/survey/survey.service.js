// Someday this will be a service
export default {
  name: 'My survey',
  questions: [
    {
      id: 1,
      text: 'What is your name?',
      type: 'text'
    },
    {
      id: 2,
      text: 'What is your favorite season?',
      type: 'dropdown',
      data: {
        options: [
          {
            id: 'primavera',
            name: 'Primavera',
            value: 'primavera'
          },
          {
            id: 'verano',
            name: 'Verano',
            value: 'verano'
          },
          {
            id: 'otono',
            name: 'Otoño',
            value: 'otono'
          },
          {
            id: 'invierno',
            name: 'Invierno',
            value: 'invierno'
          }
        ]
      }
    },
    {
      id: 3,
      text: 'What\'s your gender?',
      type: 'radio',
      data: {
        id: 'gender',
        options: [
          {
            id: 'male',
            name: 'Male',
            value: 'male'
          },
          {
            id: 'female',
            name: 'Female',
            value: 'female'
          },
          {
            id: 'undisclosed',
            name: 'Do not disclose.',
            value: 'undisclosed'
          }
        ]
      }
    }
  ]
};
