import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect }  from 'react-redux';

import { setSurvey, prevQuestion, nextQuestion } from '../../actions';
import Survey from './survey';
import mockSurvey from './survey.service';

class SurveyContainer extends Component {
  static propTypes = {
    survey: PropTypes.object.isRequired,
    setSurvey: PropTypes.func.isRequired,
    prevHandler: PropTypes.func.isRequired,
    nextHandler: PropTypes.func.isRequired
  }

  componentDidMount() {
    // To simulate getting info from service
    const timeToLoad = parseInt(Math.random() * 1000);
    setTimeout(() => { this.props.setSurvey(mockSurvey); }, timeToLoad);
  }

  render() {
    // If the question page has to be async (would be a good idea) router would be here.
    return (
      <div className="survey">
        <Survey {...this.props} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  survey: state
});

const mapDispatchToProps = dispatch => ({
  setSurvey: survey => dispatch(setSurvey(survey)),
  prevHandler: () => dispatch(prevQuestion()),
  nextHandler: () => dispatch(nextQuestion())
});

export default connect(mapStateToProps, mapDispatchToProps)(SurveyContainer);
