import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Switch, Route, withRouter } from 'react-router';

import ProgressBar from '../progress-bar/progress-bar';
import QuestionPage from '../question-page/question-page.container';

class Survey extends Component {
  // todo: use shapeOf
  static propTypes = {
    survey: PropTypes.object.isRequired,
    prevHandler: PropTypes.func.isRequired,
    nextHandler: PropTypes.func.isRequired
  }

  render() {
    const { prevHandler, nextHandler, survey } = this.props;

    return (
      <div>
        <ProgressBar percent={survey.percentDone} />
        <Switch>
          <Route exact path="/" render={() => (<h1>Home</h1>)} />
          <Route exact path="/question/:id" component={QuestionPage}/>
        </Switch>
        <hr />
        <div className="buttons">
          { !survey.isFirst && <button type="button" onClick={prevHandler}>Back</button>}
          { !survey.isLast && <button type="button" onClick={nextHandler}>Next</button>}
        </div>
        <hr />
        {survey.isLast && (
          <div>
            {survey.questions.map(question => (
              <p key={question.id}>
                <strong>{question.text}</strong>: {question.answer || 'No response.'}
              </p>
            ))}
          </div>
        )}
      </div>
    );
  }
}

export default withRouter(Survey);
