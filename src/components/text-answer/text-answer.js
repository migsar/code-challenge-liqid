import React from 'react';

export const TextAnswer = ({onAnswer}) => (
  <input
    type="text"
    placeholder="Enter your answer here."
    onChange={e => onAnswer(e.target.value)}
  />
);
