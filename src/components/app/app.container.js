import React, { Component } from 'react';
import SurveyContainer from '../survey/survey.container';

class App extends Component {
  render() {
    return (
      <SurveyContainer />
    );
  }
}

export default App;
