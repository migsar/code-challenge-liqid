export default {
  container: {
    border: '2px solid #6600ff',
    width: '100%',
    height: '20px',
    position: 'relative'
  },
  bar: ({percent}) => ({
    backgroundColor: '#aaffff',
    width: `${percent}%`,
    height: '20px',
    position: 'absolute'
  })
};
