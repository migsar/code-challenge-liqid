import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { FelaComponent as F } from 'react-fela';

import styles from './progress-bar.style';

class ProgressBar extends Component {
  static propTypes = {
    percent: PropTypes.number
  };

  static defaultProps = {
    percent: 0
  };

  render() {
    return (
      <F style={styles.container}>
        <F percent={this.props.percent} style={styles.bar} />
      </F>
    );
  }
}

export default ProgressBar;
