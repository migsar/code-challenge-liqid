import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect }  from 'react-redux';

import { answerQuestion } from '../../actions';
import { Question } from '../question/question';
import { Answer } from '../answer/answer';

class QuestionPageContainer extends Component {
  static propTypes = {
    survey: PropTypes.object.isRequired,
    match: PropTypes.object.isRequired,
    answerQuestion: PropTypes.func.isRequired
  }

  onAnswer = answer => {
    const { match: { params }, answerQuestion } = this.props;

    answerQuestion(params.id)(answer);
  }

  render() {
    const { match: { params }, survey: { questions } } = this.props;

    if (!questions) return null;

    const question = questions[parseInt(params.id)];

    return (
      <div>
        <Question question={question.text} />
        <Answer type={question.type} data={question.data} onAnswer={this.onAnswer} />
      </div>
    );
  }
}

const mapStateToProps = state => ({
  survey: state
});

const mapDispatchToProps = dispatch => ({
  answerQuestion: question => answer => dispatch(answerQuestion(question, answer)),
});

export default connect(mapStateToProps, mapDispatchToProps)(QuestionPageContainer);
