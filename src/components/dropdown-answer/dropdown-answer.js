import React from 'react';

export const DropdownAnswer = ({data}) => (
  <select>
    {data.options.map(option => (
      <option key={option.value} value={option.value}>{option.name}</option>
    ))}
  </select>
);
