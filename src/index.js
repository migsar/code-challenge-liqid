import React from 'react';
import ReactDOM from 'react-dom';
import { Router } from "react-router";
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import createBrowserHistory from "history/createBrowserHistory";
import { createRenderer } from 'fela';
import { RendererProvider } from 'react-fela';

import App from './components/app/app.container';
import rootReducer from './reducers';

const history = createBrowserHistory()
const store = createStore(rootReducer(history));
const renderer = createRenderer();

const app = (
  <Provider store={store}>
    <RendererProvider renderer={renderer}>
      <Router history={history}>
        <App />
      </Router>
    </RendererProvider>
  </Provider>
);

ReactDOM.render(app, document.getElementById('root'));
