import { ACTIONS } from '../actions';

const { SET_SURVEY, PREV_QUESTION, NEXT_QUESTION, ANSWER_QUESTION } = ACTIONS;

const initialState = {
  title: null,
  questions: null,
  currentQuestion: null,
  isFirst: null,
  isLast: null,
  length: null,
  percentDone: null
};

const getQuestionPath = questionNumber => `/question/${questionNumber}`;

// The reducer was wrapped to have the same history then the router
const survey = history => (state = initialState, action) => {
  const { currentQuestion, length, isFirst, isLast } = state;

  // We are deconstructing here but some will be undefinded
  const { survey, question, answer } = action;

  switch(action.type) {
    case SET_SURVEY:
      const { name, questions } = survey;

      // todo: not considering edge cases
      // like 0 length survey
      history.push(getQuestionPath(0));

      return {
        ...state,
        title: name,
        questions: questions,
        currentQuestion: 0,
        percentDone: parseInt( 100 / questions.length ),
        length: questions.length,
        isFirst: true,
        isLast: questions.length === 1
      };
    case NEXT_QUESTION:
      // It should let the user know
      if (isLast) {
        return state;
      }

      const nextQuestion = currentQuestion + 1;
      history.push(getQuestionPath(nextQuestion));

      return {
        ...state,
        currentQuestion: nextQuestion,
        percentDone: parseInt( (nextQuestion + 1) * 100 / length ),
        isFirst: false,
        isLast: nextQuestion === (length - 1)
      };
    case PREV_QUESTION:
      // It should let the user know
      if (isFirst) {
        return state;
      }

      const prevQuestion = currentQuestion - 1;
      history.push(getQuestionPath(prevQuestion));

      return {
        ...state,
        currentQuestion: prevQuestion,
        percentDone: parseInt( (prevQuestion + 1) * 100 / length ),
        isFirst: prevQuestion === 0,
        isLast: false
      };
    case ANSWER_QUESTION:
      const { questions: q } = state;
      // Modify nested property
      q[question].answer = answer;

      return state;
    default:
      return state;
  }
};

export default survey;
