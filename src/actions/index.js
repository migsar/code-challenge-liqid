export const ACTIONS = {
  SET_SURVEY: 'SET_SURVEY',
  PREV_QUESTION: 'PREV_QUESTION',
  NEXT_QUESTION: 'NEXT_QUESTION',
  ANSWER_QUESTION: 'ANSWER_QUESTION'
};

export const setSurvey = survey => ({
  type: ACTIONS.SET_SURVEY,
  survey
});

export const prevQuestion = () => ({
  type: ACTIONS.PREV_QUESTION,
});

export const nextQuestion = () => ({
  type: ACTIONS.NEXT_QUESTION,
});

export const answerQuestion = (question, answer) => ({
  type: ACTIONS.ANSWER_QUESTION,
  question,
  answer
});
